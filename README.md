# Hunter Assassin #

## Table of content

* Introduction to Hunter Assassin
* How to play Hunter Assassin
* What's Different about My Hunter Assassin?
* About the Code


	
## Introduction to Hunter Assassin

Hunter Assassin is actually an existing mobile game by Ruby Game Studio. Its fun and unique gameplay is what attracted so many users around the world including myself.  

As the title implies, Hunter Assassin lets you play the role as a quiet killer hiding under the shadows avoiding from being seen by any of the armed guards. Your objective is to elliminate all of the scouting guards without getting killed. You'll have to use your surroundings to hide from the flashlights of the guards. But becareful, some actions might alert the guards to come chasing after you.



## How to Play Hunter Assassin

#### Move Assassin
The player becomes the assassin. The assassin is always spawned on the bottem center of the map facing up. The fun part about Hunter Assassin is that the only control you need is the click of a mouse. You can click where you would like to go and the assassin will automatically face and walk towards the destination in a straight line. However when an obstacle blocks the path, you cannot proceed furthur. You will have set another destination just by clicking somewhere else. You can also change your destination even though your not there yey. Basically the assassin will always try to reach the latest point you clicked on.

#### Kill Guard
The assassin uses a knife. Therefore, it can only attack the guards through contact. If you approach a guard close enough, you'll be able to kill it successfuly. When a guard is killed, a red skull icon and some text will apear briefly on the center of your screen.

#### Guards Shoot
Guards are armed with guns. They can shoot you whenever you are inside their range, which is the area that the flashlights reach. When you are shown by the flashlight, the flashlight that found you will turn red, and an exclamation mark will apear above the guard's head. The guard will shoot and reload until you are dead or out of his range. When the assassin is shot, the sprite itself and the whole screen will become bloody.


#### Guards Alert
Guards can communicate through a radio channel. So when a guard sees the assassin or gets killed, all of the other guards will be alert for a certain amount of time. When alert, a question mark will apear above the guard's heads and they will rush towards your position faster than usual. However they also cannot pass through walls nor go around them. They will just stop when confronted by an obstacle.



## What's Different about My Hunter Assassin?

So by now you might be wondering 'Is it any different from the original game?'. On the surface, the characters are identical. The assassin and the guards are directly from the original game. The map textures are different and the additional effects such as killing signs and bloody screens are actually my own idea. (I even think that the rich sound effects and the addition of the announcer's voice made it a little bit fancier than the original.) 

However the real difference is in the rendering of the map. The original game runs in levels where the map is all pre-made stages. I could have just copied some stages and implement it. However I wanted to make a more general platform that can be very user friendlly. So I decided to make a map generater that randomly generates obstacles depending on the user's presets.

On the top left there are everything the user can modify.

#### Restart
The restart button is the big button with a round arrow in it. When pressed, it stops all actions from the previous stage and creates a new game based on the user settings. The restart button is used to modify maps or to start a new game after victory/defeat.

#### Settings
The user settings are on right side of the restart button. There is one default button and three preset sliders. You can modify these three variables using the sliders.
* map size : change map size
* map complexity : change the number of obstacle blocks
* guard number : change the number of guards

The default button is the button half the size of the restart button with a DEF written on it. When pressed, the user presets will be changed into the reccommended values. These are the values I tested and found to be at an appropriate difficulty. I recommend you to play with this setting a bit before trying out the presets.

#### Status Bar
The status bar is actually not something that is adjustable but instead is for showing the user two essential values. That is life, and  progress. The life bar is the thick red one on the top. It obviously represents the life of the assassin. It will decrease when shot and when it reaches 0, the game will end and the defeat banner and voice will apear. On the other hand, the thin blue other one is the progress bar. It starts at 0 and increases as you kill the guards. When all is killed, the victory banner and voice will apear.



## About the Code

In here, I will briefly introduce the ideas and some featurs of the code. For detailed looks, I actually spent a lot of time organizing the code, so I think the code itself will show the flow of work better than words.

#### Libraries
I used two libraries. I used the Sound library to implement the background music and all the other sound effects. Also I used ControlP5 library to make the sliders and buttons for the user presets.

#### Classes
There is a abstract class called Sprite. The Sprite class is extended by Assassin, Guard and Block classes. Each class constructs an object and draws the sprite applying the correct position, size, rotation, additional effect(such as flashlights for guards), etc.. Also has its own methods mainly to communicate with each other. There is also two more classes named guardGenerater and mapGenerater. These generaters are uses to create number of objects compactly and draw them in random locations. For instance, the mapGenerater creates the map randomly based on the user presets: map size and map complexity. Also the guardGenerator randomly spawns guards also based on the preset. The idea started inspired by the factory design pattern, but I'm not sure now if it is actually a factory or not. Finally these objects are all drawn repeatedly in the draw() function.

#### Stuff I'm Proud About
I'm a bit proud of myself on few things, even if it might not be a big deal for anyone else.

Starting from the random movement of guards. I was actually told not to waste a lot of energy on making the bots. However without random movement, the game itself became so loose and boring. Even though it's called hunter assassin, I can proudly say that the guard is the spotlight of the code. The fact that I managed to create a somewhat natural looking movement for the guards really makes me feel good. Also, it does not just randomly move. In case of alert, it chases down the assassin in higher speed and I take pride in that too.

If you look closely at the flashlight, it gets blocked by obstacles so that the assassin could hide in the shadows. This was actually the hardest part to ideate in the whole program. And more importantly, if I weren't able to pull this off, it would be no where near hunter assassin. The thing I did was splitting up the lights into tiny pieces and calculating the value considering the walls. It turned out lookin pretty nice and I had to mention about it here.


#### Bugs & Future Improvements
As this is the first complete version of the game, there are much much more to do. It just has become playable at this moment. There are already some known bugs that I couldn't/didn't fix and some improvements I would like to make.
* flashlight peeking through diagonally meeting walls
* able to kill guard across diagonally meeting walls
* assassin avoiding obstacles to reach destination when mouse clicked
* guard smashing into walls when chasing
* guards only able to move north,west,south,east
* add moving feet
* add shooting fire to guard's gun
* add timer and measure record
* add different background textures



## Thank you for reading such a long text!