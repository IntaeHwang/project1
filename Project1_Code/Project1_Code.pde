/*
	Project 1
	Name of Project: Hunter Assassin
	Author: Intae Hwang
	Date: 5/27
*/

// your code down here
// feel free to crate other .pde files to organize your code
import processing.sound.*;
import controlP5.*;

//_____User Default Settings_____
//Map
int MAP_SIZE = 15; //Recommended Value : 15 Range : 6~30
int MAP_COMPLEXITY = 20; //Recommended Value : 20 Range : 0~80 *High values might trap sprites and lag game
float BLOCK_SIZE;
//Assassin
int ASSASSIN_LIFE = 5; //Recommended Value : 5
float ASSASSIN_SPEED;
//Guard
int GUARD_NUMBER = 5; //Recommended Value : 5 Range : 1~15
int GUARD_NUMBER_THIS;
float GUARD_WALK_SPEED; //Recommended Value : 1
float GUARD_RUN_SPEED; //Recommended Value : 3
int GUARD_RUN_TIME = 50; //Recommended Value : 50
int GUARD_RELOAD_TIME = 8; //Recommend Value : 8
//Light
int LIGHT_RADIUS_RATIO = 3; //Recommended Value : 3
int LIGHT_QUALITY = 100; //Recommended Value : 100
float LIGHT_ANGLE = PI/2; //Recommended Value : PI/2
int LIGHT_OPACITY = 50; //Recommended Value : 50
//GUI
int KILL_SIGN_TIME = 10;
//________________________________

//_____Objects_____
Assassin assassin;
GuardGenerater guards;
MapGenerater map;
//_________________

//_____Sounds_____
SoundFile bgm;
SoundFile knife;
SoundFile gunshot;
SoundFile radio;
SoundFile surprise;
SoundFile footsteps;
SoundFile victory;
SoundFile defeat;
//_________________

//_____Images_____
PImage victoryText;
PImage defeatText;
PImage restartImg;
PImage skull;
//________________

//_____GUI_____
ControlP5 cp5;

//_____Global Variables_____
boolean runAll;
int runTimeLeft;
int assassinLifeLeft;
int score;
boolean endGame;
int killSignTimeLeft;
//__________________________

void setup()
{
	// your code
	size (900, 900);

	//Sound Load
	bgm = new SoundFile (this, "bgm.mp3");
	knife = new SoundFile (this, "knife.mp3");
	gunshot = new SoundFile (this, "gunshot.mp3");
	radio = new SoundFile (this, "radio.mp3");
	surprise = new SoundFile (this, "surprise.mp3");
	footsteps = new SoundFile (this, "footsteps.mp3");
	victory = new SoundFile (this, "victory.mp3");
	defeat = new SoundFile (this, "defeat.mp3");

	//Image load
	victoryText = loadImage("victory.png");
	defeatText = loadImage("defeat.png");
	restartImg = loadImage("restart.png");
	skull = loadImage("skull.png");

	//GUI load
	cp5 = new ControlP5(this);
	cp5.addSlider("MAP_SIZE").setPosition(75,0).setRange(6,30).setSize(75,16)
	.setColorValue(color(0)).setColorActive(color(255))
	.setColorForeground(color(255,255,255)).setColorBackground(color(0,0,0));

	cp5.addSlider("MAP_COMPLEXITY").setPosition(75,16).setRange(0, 80).setSize(75,17)
	.setColorValue(color(0,0,0)).setColorActive(color(255))
	.setColorForeground(color(255,255,255)).setColorBackground(color(0,0,0));

	cp5.addSlider("GUARD_NUMBER").setPosition(75,33).setRange(1, 15).setSize(75,17)
	.setColorValue(color(0,0,0)).setColorActive(color(255))
	.setColorForeground(color(255,255,255)).setColorBackground(color(0,0,0));

	cp5.addButton("DEF").setValue(0).setPosition(50,0).setSize(25,50)
	.setColorValue(color(255)).setColorActive(color(0))
	.setColorForeground(color(0)).setColorBackground(color(0));

	//Start Game
	restart();
	bgm.loop();
}

//Change into Recommended Value
void DEF()
{ 
	cp5.getController("MAP_SIZE").setValue(15);
	cp5.getController("MAP_COMPLEXITY").setValue(20);
	cp5.getController("GUARD_NUMBER").setValue(5);
}

void draw()
{
	// your code
	background(120);

	//draw sprites
	guards.draw();
	if(assassinLifeLeft>0) assassin.draw();
	map.draw();

	//run time measure
	if(runAll==true) runTimeLeft--;
	else runTimeLeft = GUARD_RUN_TIME;
	if(runTimeLeft<=0) runAll=false;

	//announcments
	if(score == GUARD_NUMBER_THIS && endGame==false)
	{
		victory.play();
		endGame = true;
	}
	if(assassinLifeLeft <= 0 && endGame==false)
	{
		defeat.play();
		endGame = true;
	}

	//footsteps
	if(runTimeLeft == GUARD_RUN_TIME-1 && score != GUARD_NUMBER_THIS) radio.play();

	if((assassin.isStill() || assassinLifeLeft<=0) && footsteps.isPlaying()==true ) footsteps.pause();
	else if(footsteps.isPlaying()==false && assassinLifeLeft>0)
	{
		if(mouseX>150 || mouseY>50) footsteps.loop();
	}

	//bloody screen
	if(!endGame)
	{
		fill(255,0,0,50*(1-(float(assassinLifeLeft)/ASSASSIN_LIFE)));
		noStroke();
		rectMode(CORNER);
		rect(0,0,900,900);
	}

	//draw kill sign
	if(killSignTimeLeft>0)
	{
		imageMode(CENTER);
		tint(255, 175);
		image(skull, 450, 450, skull.width/2, skull.height/2);
		tint(255, 255);
		killSignTimeLeft--;
	}

	//blur
	if(endGame  && assassinLifeLeft>0)
	{
		radio.stop();
		
		fill(255,225,0,100);
		noStroke();
		rectMode(CORNER);
		rect(0,0,900,900);

		imageMode(CENTER);
		image(victoryText, 450, 450, victoryText.width*1.5, victoryText.height*1.5); 
	}
	else if(endGame  && assassinLifeLeft<=0)
	{
		radio.stop();

		fill(255,0,0,100);
		noStroke();
		rectMode(CORNER);
		rect(0,0,900,900);

		imageMode(CENTER);
		image(defeatText, 450, 450, defeatText.width*1.5, defeatText.height*1.5); 
	}

	//Draw Restart Button
	imageMode(CORNER);
	image(restartImg, 0, 0, 50, 50);

	//Draw Life bar
	fill(0,0,0);
	noStroke();
	rectMode(CORNER);
	rect(0,50,150,25);

	fill(255,0,0);
	noStroke();
	rectMode(CORNER);
	rect(0,50,150*assassinLifeLeft/ASSASSIN_LIFE,25);

	//Draw Score bar
	fill(0,0,0);
	noStroke();
	rectMode(CORNER);
	rect(0,75,150,6);

	fill(0,0,255);
	noStroke();
	rectMode(CORNER);
	rect(0,75,150*score/GUARD_NUMBER_THIS,6);
}

void restart()
{
	//Intialize Global Variables
	runAll = false;
	runTimeLeft = GUARD_RUN_TIME;
	assassinLifeLeft = ASSASSIN_LIFE;
	score = 0;
	endGame = false;
	killSignTimeLeft = 0;

	//Initializing Dependent Constants
	BLOCK_SIZE = 900/(MAP_SIZE+1);
	ASSASSIN_SPEED = 5*15/MAP_SIZE;
	GUARD_WALK_SPEED = 1*15/float(MAP_SIZE);
	GUARD_RUN_SPEED = 4*15/float(MAP_SIZE);
	GUARD_NUMBER_THIS = GUARD_NUMBER;

	//Stop All Sounds
	knife.stop();
	gunshot.stop();
	radio.stop();
	surprise.stop();
	footsteps.stop();
	victory.stop();
	defeat.stop();

	//Load New Graphics
	map = new MapGenerater (MAP_SIZE, MAP_COMPLEXITY);
	assassin = new Assassin (width/2, height-width/(MAP_SIZE+1), BLOCK_SIZE);
	guards = new GuardGenerater (GUARD_NUMBER_THIS);
}

void mousePressed()
{
	if(mouseX<=50 && mouseY<=50) restart();
	else if(!(50<mouseX && mouseX<=150 && mouseY<=50)) assassin.setDestination(mouseX, mouseY);
}

abstract class Sprite
{
	protected float x, y;

	Sprite (float x, float y)
	{
		this.x = x;
		this.y = y;
	}
}

class Assassin extends Sprite
{
	//Basic Variables
	private float w, h;
	private PImage [] imgs;

	//Unique Variables
	private float destinationX, destinationY;
	private float v = ASSASSIN_SPEED;
	private float rotation=0;
	PVector direction = new PVector (0, 0);

	//Constructer
	Assassin (float x, float y, float height)
	{
		super(x,y);
		imgs = new PImage [2];
		imgs[0] = loadImage ("assassin.png");
		h = height;
		float ratio = float(imgs[0].width)/imgs[0].height;
		w = h*ratio;
		destinationX = x;
		destinationY = y;
	}

	boolean isStill()
	{
		return (x==destinationX && y==destinationY);
	}

	//Move and Draw Assassin
	void draw()
	{
		float prevX = x;
		float prevY = y;
		float vX, vY;
		direction.x = destinationX - x;
		direction.y = destinationY - y;
		if(dist(0,0,direction.x,direction.y)<v)
		{
			x=destinationX;
			y=destinationY;
		}
		else
		{
			if(direction.x>=0) rotation = PI/2 + atan(direction.y/direction.x);
			if(direction.x<0) rotation = PI*3/2 + atan(direction.y/direction.x);
			vX = v*direction.x/dist(0,0,direction.x,direction.y);
			vY = v*direction.y/dist(0,0,direction.x,direction.y);
			if(x != destinationX) x = x + vX;
			if(y != destinationY) y = y + vY;
		}
		if(map.touchingMap(x,y))
		{
			x=prevX;
			y=prevY;
			destinationX = x;
			destinationY = y;
		}

		//Drawing Assassin
		imageMode(CENTER);
		translate(x, y);
		rotate(rotation);
		tint(255,51*assassinLifeLeft,51*assassinLifeLeft);
		image(imgs[0], 0, 0, w, h);
		tint(255);
		rotate(-rotation);
		translate(-x, -y);
	}

	//Set Destination to Where Mouse was Clicked
	void setDestination(float x, float y)
	{
		destinationX = x;
		destinationY = y;
	}

	//X Getter
	float getX()
	{
		return x;
	}

	//Y Getter
	float getY()
	{
		return y;
	}
}

class Guard extends Sprite
{
	//Basic Variables
	private float w, h;
	private PImage [] imgs;

	//Unique Variables
	private float vX = GUARD_WALK_SPEED;
	private float vY = GUARD_WALK_SPEED;;
	private float DEFAULT_ROTATION = PI*3/4;
	private float rotation = DEFAULT_ROTATION;
	private boolean isAlive = true;
	private boolean myLightRed;
	private boolean shot;
	private int reloadTimeLeft = GUARD_RELOAD_TIME;
	private PImage exMark;
	private PImage qMark;

	//Direction Constants
	float RIGHT = 0+DEFAULT_ROTATION;
	float LEFT = PI+DEFAULT_ROTATION;
	float UP = 3*PI/2+DEFAULT_ROTATION;
	float DOWN = PI/2+DEFAULT_ROTATION;

	//Constructer
	Guard (float x, float y, float width)
	{
		super(x,y);
		imgs = new PImage [2];
		imgs[0] = loadImage ("guard.png");
		w = width;
		float ratio = float(imgs[0].height)/imgs[0].width;
		h = w*ratio;

		exMark = loadImage ("exmark.png");
		qMark = loadImage ("qmark.png");
	}

	//Move and Draw Guard
	void draw()
	{
		float assassinX = assassin.getX();
		float assassinY = assassin.getY();
		float d = dist(x,y,assassinX,assassinY);

		if(myLightRed && runTimeLeft == GUARD_RUN_TIME-1) surprise.play();

		if(d<w && assassinLifeLeft>0) //Killed
		{
			knife.play();
			//println("Killed Guard");
			score++;
			isAlive = false;
			runAll = true;
			killSignTimeLeft = KILL_SIGN_TIME;
		}

		//Adjusting vX, vY
		if(runAll == false && map.touchingMap(x,y)) //Bouncing off Walls while Walking
		{
			if(rotation == RIGHT)
			{
				vX = -vX;
				rotation = LEFT;
			}
			else if(rotation == LEFT)
			{
				vX = -vX;
				rotation = RIGHT;
			}
			else if(rotation == UP)
			{
				vY = -vY;
				rotation = DOWN;
			}
			else if(rotation == DOWN)
			{
				vY = -vY;
				rotation = UP;
			}
		}
		else if(runAll == true && map.touchingMap(x,y)) //Bouncing on Walls while Running
		{
			if(rotation == RIGHT || rotation == LEFT) vX = -vX;
			else vY = -vY;
		}
		else if(runAll == true && myLightRed == false && !endGame) //Chasing
		{
			if(abs(assassinX-x) > abs(assassinY-y))
			{
				if(assassinX>x)
				{
					rotation = RIGHT;
					vX = GUARD_RUN_SPEED;
				}
				else
				{
					rotation = LEFT;
					vX = -GUARD_RUN_SPEED;
				}
			}
			else
			{
				if(assassinY>y)
				{
					rotation = DOWN;
					vY = GUARD_RUN_SPEED;
				}
				else
				{
					rotation = UP;
					vY = -GUARD_RUN_SPEED;
				}
			}
		}
		else if(runAll == false && myLightRed == false || endGame) //Walking Around
		{
			//Speed Initialize
			if(rotation==RIGHT) vX = GUARD_WALK_SPEED;
			else if(rotation==LEFT) vX = -GUARD_WALK_SPEED;
			else if(rotation==UP) vY = -GUARD_WALK_SPEED;
			else vY = GUARD_WALK_SPEED;

			//Random Movement________________________
			if(int(random(0,100))==0)
			{
				if(rotation==RIGHT || rotation==LEFT)
				{

					if(round(random(0,1))==0)
					{
						rotation = UP;
						if(vY>0) vY = -vY; 
					}
					else
					{
						rotation = DOWN;
						if(vY<0) vY = -vY; 
					}
				}
				else
				{
					if(round(random(0,1))==0)
					{
						rotation = RIGHT;
						if(vX<0) vX = -vX;
					}
					else 
					{
						rotation = LEFT;
						if(vX>0) vX = -vX;
					}
				}
			}
			//______________________________________
		}
		else //Shooting
		{
			vX = 0;
			vY = 0;
			reloadTimeLeft--;
			if(reloadTimeLeft <= 0 && assassinLifeLeft>0)
			{
				gunshot.play();
				//println("Shot");
				shot = true;
				reloadTimeLeft = GUARD_RELOAD_TIME;
				assassinLifeLeft--;
			}
			else shot = false;
		}

		//Adding vX, vY
		if(rotation==RIGHT || rotation==LEFT) x = x+vX;
		if(rotation==UP || rotation==DOWN) y= y+vY;

		//Drawing Guard
		imageMode(CENTER);
		translate(x, y);
		rotate(rotation);
		image(imgs[0], 0, 0, w, h);
		rotate(-rotation);
		translate(-x, -y);

		//Draw Exclamation Mark
		if(runAll && myLightRed==true && !endGame)
		{
			imageMode(CENTER);
			image(exMark, x-(5*15/float(MAP_SIZE)), y-(40*15/float(MAP_SIZE)), exMark.width/4*15/float(MAP_SIZE), exMark.height/4*15/float(MAP_SIZE));
		}

		//Draw Question Mark
		if(runAll && myLightRed==false && !endGame)
		{
			imageMode(CENTER);
			image(qMark, x-(5*15/float(MAP_SIZE)), y-(40*15/float(MAP_SIZE)), qMark.width/4*15/float(MAP_SIZE), qMark.height/4*15/float(MAP_SIZE));
		}
	}

	//Draw Light
	void drawLight()
	{
		//Light Dimension Variables
		float start = 3*PI/2 - LIGHT_ANGLE/2 - PI/4 + rotation;
		float end = 3*PI/2 + LIGHT_ANGLE/2 - PI/4 + rotation;
		float LIGHT_RADIUS = w*LIGHT_RADIUS_RATIO;
		float delta = LIGHT_ANGLE/LIGHT_QUALITY;
		float [] radius = new float [LIGHT_QUALITY];
		
		//Assassin Interaction Variables
		float assassinX = assassin.getX();
		float assassinY = assassin.getY();
		float d = dist(x,y,assassinX,assassinY);
		boolean touchingAssassin = false;

		//Finding the radius for each delta arc
		for(int i=0; i<LIGHT_QUALITY; i++)
		{
			for(int r=0;r<=LIGHT_RADIUS;r++)
			{
				float p = x-r*cos(start+delta*i-PI);
				float q = y-r*sin(start+delta*i-PI);
				if(map.touchingMap(p,q))
				{
					radius[i] = r;
					break;
				}
				if(r==LIGHT_RADIUS)
				{
					radius[i] = r;
				}
			}
		}

		//Finding if Light is touching Assassin 
		for(int i=0; i<LIGHT_QUALITY; i++)
		{
			PVector v1 = new PVector(cos(start+delta*i) , sin(start+delta*i));
			PVector v2 = new PVector(cos(start+delta*(i+1)+PI/180), sin(start+delta*(i+1)+PI/180));
			PVector v = new PVector(assassinX-x,assassinY-y);
			if(PVector.angleBetween(v1,v)<delta+PI/180 && PVector.angleBetween(v,v2)<delta+PI/180)
			{
				if(d <= radius[i]) touchingAssassin = true;
			}
		}

		//Drawing Light
		for(int i=0; i<LIGHT_QUALITY; i++)
		{
			noStroke();
			if(touchingAssassin && assassinLifeLeft>0)
			{
				if(shot == true)
				{
					fill(255,255,200,255*LIGHT_OPACITY/100);
				}
				else fill(255,0,0,255*LIGHT_OPACITY/100);
				runAll = true;
				myLightRed = true;
			}
			else
			{
				fill(255,255,255,255*LIGHT_OPACITY/100);
				myLightRed = false;
			}
			arc(x,y,2*radius[i],2*radius[i],start+delta*i,start+delta*(i+1)+PI/180);
		}
	}

	//isAlive Getter
	boolean isAlive()
	{
		return isAlive;
	}
}

//Generates Guards at Random Position
class GuardGenerater
{
	private int number;
	private float [] spawnXY;
	private Guard [] guards;

	GuardGenerater(int n)
	{
		number = n;
		spawnXY = new float [2];
		guards = new Guard [number];
		for(int i=0; i<number; i++)
		{
			spawnXY = map.spawnRandom();
			Guard guard = new Guard(spawnXY[0],spawnXY[1],BLOCK_SIZE);
			guards[i] = guard;
		}
	}

	void draw()
	{
		for(int i=0; i<number; i++)
		{
			if(guards[i].isAlive())
			{
				guards[i].drawLight();
			}
		}
		for(int i=0; i<number; i++)
		{
			if(guards[i].isAlive()) guards[i].draw();
		}
	}
}

class Block extends Sprite
{
	private float w, h;
	private PImage [] imgs;

	Block (float x, float y, float width)
	{
		super(x,y);
		imgs = new PImage [2];
		imgs[0] = loadImage ("Wood.png");
		w = width;
		h = w;
	}

	void draw()
	{
		imageMode(CENTER);
		image(imgs[0], x, y, w, h);
	}

	void drawShadow()
	{
		fill(0);
		noStroke();
		rectMode(CENTER);
		rect(x,y,w+5,w+5);
	}
}

class MapGenerater
{
	private float w;
	private float [] xpos;
	private float [] ypos;
	private boolean [][] notEmpty;
	private boolean [][] spawnable;
	private boolean [] random;
	private int scale;
	private Block [][] blocks;

	MapGenerater (int scale, int complexity)
	{
		this.scale = scale;
		w = width/(scale+1);
		xpos = new float[scale+2];
		ypos = new float[scale+2];
		notEmpty = new boolean [scale+2][scale+2];
		spawnable = new boolean [scale+2][scale+2];
		random = new boolean [100];
		blocks = new Block [scale+2][scale+2];
		for(int i=0;i<scale+2;i++)
		{
			xpos[i] = w*i;
			ypos[i] = w*i;
		}
		for(int i=0; i<100; i++)
		{
			if(i<complexity) random[i] = true;
			else random[i] = false;
		}
		for(int i=0; i<scale+2; i++)
		{
			for(int j=0; j<scale+2; j++)
			{
				if(i==0 || j==0 || i==scale+1 || j==scale+1) notEmpty[i][j] = true;
				else if(i==1 || j==1 || i==scale || j==scale) notEmpty[i][j] = false;
				else
				{
					int index = int(random(100));
					notEmpty[i][j] = random[index];
				}
				if(notEmpty[i][j])
				{
					Block block = new Block(xpos[i], ypos[j], w);
					blocks[i][j] = block;
				}
				
			}
		}
		for(int i=0; i<scale+2; i++)
		{
			for(int j=0; j<scale+2; j++)
			{
				if(notEmpty[i][j]) spawnable[i][j] = false;
				else spawnable[i][j] = true;
				if(i==1 || j==1 || i==scale || j==scale) spawnable[i][j] = false;
				if(scale%2==0)
				{
					if(i==scale/2-2 || i==scale/2-1 || i==scale/2 || i==scale/2+1 || i==scale/2+2 || i==scale/2+3)
					{
						if(j==scale-2 || j==scale-1 || j==scale)
						{
							spawnable[i][j] = false;
						}
					}
				}
				else
				{
					if(i==scale/2-2 || i==scale/2-1 || i==scale/2 || i==scale/2+1 || i==scale/2+2)
					{
						if(j==scale-2 || j==scale-1 || j==scale)
						{
							spawnable[i][j] = false;
						}
					}
				}
			}
		}
	}

	void draw()
	{
		for(int i=0; i<scale+2; i++)
		{
			for(int j=0; j<scale+2; j++)
			{
				if(notEmpty[i][j] == true) blocks[i][j].drawShadow();
			}
		}
		for(int i=0; i<scale+2; i++)
		{
			for(int j=0; j<scale+2; j++)
			{
				if(notEmpty[i][j] == true) blocks[i][j].draw();
			}
		}
	}

	
	boolean touchingMap(float x, float y)
	{
		int i = ceil((x-w/2)/w);
		int j = ceil((y-w/2)/w);
		return notEmpty[i][j];
	}

	float[] spawnRandom()
	{
		int i = int(random(1,scale));
		int j = int(random(1, scale));
		float[] spawn = new float[2];
		while(spawnable[i][j] == false)
		{
			i = int(random(1,scale));
			j = int(random(1, scale));
		}
		spawn[0] = i*w;
		spawn[1] = j*w;
		spawnable[i][j] = false;
		return spawn;
	}
}